"use strict";

(function () {
  const updateTime = function () {
    const now = new Date();
    document.getElementById('time').innerHTML = now.toLocaleTimeString();
  };

  const keyDownHandler = function (e) {
    if (e.key.toLowerCase() !== 'f') return;

    if (document.fullscreenElement === null) {
      try {
        document.body.requestFullscreen();
      }
      catch (e) {
        console.log(e);
      }
    }
    else {
      document.exitFullscreen();
    }
  };

  document.onkeydown = keyDownHandler;
  setInterval(updateTime, 100);

})();
